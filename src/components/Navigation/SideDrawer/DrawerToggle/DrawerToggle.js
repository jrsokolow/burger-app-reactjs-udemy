/**
 * Created by sokoloj1 on 2018-02-23.
 */

import React from 'react';

import classes from './DrawerToggle.css';

const drawerToggle = (props) => (
    <div onClick={props.clicked}>MENU</div>
);

export default drawerToggle;