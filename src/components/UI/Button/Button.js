/**
 * Created by sokoloj1 on 2018-02-02.
 */

import React from 'react';
import classes from './Button.css';

const button = (props) => (
  <button className={[classes.Button, classes[props.btnType]].join(' ')} onClick={props.clicked}>{props.children}</button>
);

export default button;
